<?php
namespace Action;
use HY\Action;

class UserAction extends HYBBS {
    public $menu_action;
    public function __construct(){
		parent::__construct();
        {hook a_user_init}
        $this->menu_action = array(
            'index'=>'',
            'thread'=>'',
            'post'=>'',
            'mess'=>''
        );
        $left_menu = array('index'=>'active','forum'=>'');
		$this->v("left_menu",$left_menu);

    }

    public function Mess(){
        $this->menu_action['mess'] = 'active';
        $this->v('menu_action',$this->menu_action);
        {hook a_user_mess_1}
        if(!IS_LOGIN)
            return $this->message('登录后才能查看你自己的消息');

        $id = intval($this->_user['id']);

        {hook a_user_mess_2}
        if(empty($id))
            return $this->message('用户ID不能为空');
        $User = M("User");

        $data = $User->read($id);
        {hook a_user_mess_3}
        $Mess = S("Mess");
        $pageid=intval(X('get.pageid')) or $pageid=1;
        $mess_data = $Mess->select('*',array(
            'uid'=>$id,
            "LIMIT" => array(($pageid-1) * 10, 10),
            "ORDER"=>"atime DESC",
        ));
        {hook a_user_mess_4}
        $User=M('User');
        foreach ($mess_data as &$v) {
            $v['avatar']=$this->avatar($User->id_to_user($v['suid']));

        }

        $count = $Mess->count(array('uid'=>$id));
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);
        {hook a_user_mess_v}
        $this->v("title","消息中心");
        $this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
        $this->v('mess_data',$mess_data);
        $this->v('data',$data);
        $this->display('user_mess');
    }
    public function Post(){
        $this->menu_action['post'] = 'active';
        $this->v('menu_action',$this->menu_action);

        {hook a_user_post_1}
        $id = intval(X('get.id'));
        if(empty($id))
            return $this->message('用户ID不能为空');
        {hook a_user_post_2}
        $User = M("User");
        if(!$User->is_id($id))
            return $this->message('不存在该用户');
        $data = $User->read($id);
        {hook a_user_post_3}
        $Post = S("Post");

        $pageid=intval(X('get.pageid')) or $pageid=1;
        $post_data = $Post->select('*',array(
            'AND'=>array(
                'uid'=>$id,
                'isthread'=>0,
            ),
            "LIMIT" => array(($pageid-1) * 10, 10),
            "ORDER" => "atime DESC"
        ));
        {hook a_user_post_4}
        foreach ($post_data as &$v) {
            $v['atime']=humandate($v['atime']);
        }

        $count = $data['posts'];
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);
        {hook a_user_post_v}
        $this->v("title",$data['user'].'的帖子');
        $this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
        $this->v('post_data',$post_data);
        $this->v('data',$data);
        $this->display('user_post');

    }
    public function Thread(){
        $this->menu_action['thread'] = 'active';
        $this->v('menu_action',$this->menu_action);
        {hook a_user_thread_1}
        $id = intval(X('get.id'));
        if(empty($id))
            return $this->message('用户ID不能为空');
        {hook a_user_thread_2}
        $User = M("User");
        if(!$User->is_id($id))
            return $this->message('不存在该用户');


        $data = $User->read($id);
        {hook a_user_thread_3}
        $Thread = S("Thread");
        $pageid=intval(X('get.pageid')) or $pageid=1;
        $thread_data = $Thread->select('*',array(
            'uid'=>$id,
            "LIMIT" => array(($pageid-1) * 10, 10),
            "ORDER" => "atime DESC"
        ));
        {hook a_user_thread_4}
        foreach ($thread_data as &$v) {
            $v['atime']=humandate($v['atime']);
        }
        //print_r($thread_data);

        $count = $data['threads'];
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);
        {hook a_user_thread_v}
        $this->v("title",$data['user']."的主题");
        $this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
        $this->v('thread_data',$thread_data);
        $this->v('data',$data);
        $this->display('user_thread');
    }
    //用户首页
    public function Index(){
        $this->menu_action['index'] = 'active';
        $this->v('menu_action',$this->menu_action);
        {hook a_user_index_1}
        $id = intval(X('get.id'));
        if(empty($id))
            return $this->message('用户ID不能为空');
        {hook a_user_index_2}
        $User = M("User");
        if(!$User->is_id($id))
            return $this->message('不存在该用户');
        $data = $User->read($id);

        $data['avatar'] = $this->avatar($data['user']);
        {hook a_user_index_v}
        $this->v("title",$data['user']." 用户中心");
        $this->v('data',$data);
        $this->display('user_index');
    }
    //登录账号
    public function Login(){
        //cookie("test",34);
        {hook a_user_login_1}
        $this->v("title","登录页面");

        if(IS_GET){
            {hook a_user_login_2}
            $this->display('user_login');
        }
        elseif(IS_POST){
            $user = X("post.user");
            $pass = X("post.pass");

            $UserLib = L("User");
            {hook a_user_login_3}

            $msg = $UserLib->check_user($user);
            //检查用户名格式是否正确
            if(!empty($msg))
                return $this->message($msg);

            if(!$UserLib->check_pass($pass))
                return $this->message('密码不符合规则');
            {hook a_user_login_4}
            $User = M("User");
            if(!$User->is_user($user))
                return $this->message("账号不存在!");

            $data = $User->find("*",array('user'=>$user));
            {hook a_user_login_5}
            if(!empty($data)){
                $UserLib = L("User");
                if($data['pass'] == $UserLib->md5_md5($pass,$data['salt'])){

                    cookie('HYBBS_HEX',$UserLib->set_cookie($data));
                    $this->init_user();
                    return $this->message("登录成功 !");
                }else{
                    return $this->message("密码错误!");
                }
            }else{
                return $this->message('账号数据不存在!');
            }




        }
    }
    //注册账号
    public function Add(){
        {hook a_user_add_1}
        $this->v("title","注册用户");
        if(IS_GET){
            {hook a_user_add_2}
            $this->display('user_add');
        }
        elseif(IS_POST){
            $user = X("post.user");
            $pass1 = X("post.pass1");
            $pass2 = X("post.pass2");
            $email = X("post.email");
            {hook a_user_add_3}
            if($pass1 != $pass2)
                return $this->message("两次密码不一致");

            $UserLib = L("User");
            $msg = $UserLib->check_user($user);
            //检查用户名格式是否正确
            if(!empty($msg))
                return $this->message($msg);

            if(!$UserLib->check_pass($pass1))
                return $this->message('密码不符合规则');

            {hook a_user_add_4}

            $msg = $UserLib->check_email($email);

            if(!empty($msg))
                return $this->message($msg);

            {hook a_user_add_5}
            $User = M("User");
            if($User->is_user($user))
                return $this->message("账号已经存在!");

            if($User->is_email($email))
                return $this->message("邮箱已经存在!");


            {hook a_user_add_6}
            $User->add_user($user,$pass1,$email);
            return $this->message("账号注册成功");





        }
    }
    public function ava(){
        {hook a_user_ava_1}
        $this->v("title","更改头像");
        if(!IS_LOGIN) return $this->message("请登录后操作!");

        {hook a_user_ava_2}
        $id = $this->_user['id'];
        if(empty($id)) return $this->message("请重新登录!");

        L("Upload");
        {hook a_user_ava_3}
        $upload = new \Lib\Upload();
        $upload->maxSize   =     3145728 ;// 设置附件上传大小  3M
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     INDEX_PATH . 'upload/avatar/'; // 设置附件上传根目录
        $upload->saveExt    =   "jpg";
        $upload->replace    =   true;
        $upload->autoSub    =   false;
        $upload->saveName   =   md5($this->_user['user'].C("MD5_KEY"));
        {hook a_user_ava_4}
        $info   =   $upload->upload();
        if(!$info)
            return $this->message("请重新登录!");

        {hook a_user_ava_5}
        $image = new \Lib\Image();
        $image->open(INDEX_PATH . 'upload/avatar/'.$upload->saveName.".jpg");
        // 生成一个缩放后填充大小150*150的缩略图并保存为thumb.jpg
        $image->thumb(250, 250,$image::IMAGE_THUMB_CENTER)->save(INDEX_PATH . 'upload/avatar/'.$upload->saveName."-a.jpg");
        $image->thumb(150, 150,$image::IMAGE_THUMB_CENTER)->save(INDEX_PATH . 'upload/avatar/'.$upload->saveName."-b.jpg");
        $image->thumb(50  , 50,$image::IMAGE_THUMB_CENTER)->save(INDEX_PATH . 'upload/avatar/'.$upload->saveName."-c.jpg");
        //$image->thumb(150, 150,\Think\Image::IMAGE_THUMB_CENTER)
        {hook a_user_ava_v}
        return $this->message("上传成功!");

    }
    public function out(){
        {hook a_user_out_v}
        $this->v("title","注销用户");
        cookie('HYBBS_HEX',null);
        $this->init_user();
        $this->message('退出成功');
    }
    public function isuser(){
        $user = X("post.user");
        $bool = M("User")->is_user($user);
        return $this->json(array('error'=>$bool));
    }
    public function isemail(){
        $email = X("post.email");
        $bool = M("User")->is_email($email);
        return $this->json(array('error'=>$bool));
    }

    {hook a_user_fun}
}
