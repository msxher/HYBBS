<?php
namespace Action;
use HY\Action;

class ThreadAction extends HYBBS {
    public function __construct(){
		parent::__construct();


		$left_menu = array('index'=>'active','forum'=>'');
		$this->v("left_menu",$left_menu);
        {hook a_thread_init}
	}
    public function index(){
        echo 'ThreadIndex';
        {hook a_thread_index_1}
    }
    public function _empty(){
        {hook a_thread_empty_1}
        if(IS_GET){
            $pageid=intval(X('get.pageid')) or $pageid=1;

            $id = intval(METHOD_NAME);

            {hook a_thread_empty_2}
            $Thread = S("Thread");

            $thread_data = $Thread->find("*",array('id'=>$id));
            if(empty($thread_data))
                return $this->message("不存在该主题");

            {hook a_thread_empty_3}
            $User = M("User");
            $thread_data['user']=$User->id_to_user($thread_data['uid']);
            $thread_data['avatar']=$this->avatar($thread_data['user']);

            {hook a_thread_empty_4}
            $Post = S("Post");
            $PostData = $Post->find("*",array('AND'=>array('tid'=>$id,'isthread'=>1)));

            if(empty($PostData))
                return $this->message("文章内容没有找到");

            {hook a_thread_empty_5}
            $PostList = $Post->select('*',array(
                'AND'=>array(
                    'tid'=>$id,
                    'isthread'=>0
                ),
                "LIMIT" => array(($pageid-1) * 10, 10),


            ));

            $i = 0;
            foreach ($PostList as &$v) {
                $v['user']=$User->id_to_user($v['uid']);
                $v['atime_str']=humandate($v['atime']);
                $v['key'] = (($pageid-1)*10) + (++$i);
                $v['avatar']=$this->avatar($v['user']);
            }


            $count = $thread_data['posts'];
    		$count = (!$count)?1:$count;
    		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);
            {hook a_thread_empty_v}
            $this->v("title",$thread_data['title']);
            $this->v("post_data",$PostData);
            $this->v("pageid",$pageid);
            $this->v("page_count",$page_count);
            $this->v("thread_data",$thread_data);
            $this->v("PostList",$PostList);
            $this->display('thread_index');
        }elseif(IS_POST){

        }

    }
    public function del(){

        if(!IS_LOGIN)
            $this->json(array('error'=>false,'info'=>'请登录'));


        $id = intval(X("post.id"));
        $Thread = M("Thread");

        $t_data = $Thread->read($id);
        if(empty($t_data))
            return $this->json(array('error'=>false,'info'=>'该文章无数据'));


        //用户组不是 管理员 &&  用户不是文章作者
        if(($this->_user['group'] != C("ADMIN_GROUP")) && ($this->_user['uid'] != $t_data['uid']))
            return $this->json(array('error'=>false,'info'=>'你没有权限操作这个主题'));


        $Thread->del($id);

        if($t_data['posts']){ //存在评论
            $Post = M('Post');
            $Post->del_thread_all_post($id);
        }
        return $this->json(array('error'=>true,'info'=>'删除成功'));



    }
    {hook a_thread_fun}

}
